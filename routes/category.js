const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CategoryController = require('../controllers/category')


router.get('/', (req, res) => {
	CategoryController.getAll().then(results => res.send(results))
})




router.post('/', auth.verify, (req, res)=>{
	const arg = {
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType
	}

	CategoryController.add(arg).then(result => res.send(result))
})

router.post('/add-category', auth.verify, (req, res)=> {
	req.body.userId = auth.decode(req.headers.authorization).id;
	console.log('hello there');
	CategoryController.addCategory(req.body).then(result => res.send(result))
})



router.post('/get-categories', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    CategoryController.getCategories(req.body).then(result => res.send(result))
})



router.post('/:id', (req, res)=>{
	CategoryController.getAll({id: req.params.id})
	.then(category => res.send(category))
})


//delete 
router.delete('/:id', (req, res) => {
	const arg = {
		categoryId: req.params.id,
	}
	CategoryController.archive(arg).then(result => res.send(result))
})


module.exports = router