const express = require('express')
const cors = require('cors')
const app = express()
require('dotenv').config()
const mongoose = require('mongoose')
const port = process.env.PORT

const userRoutes = require('./routes/user')
const recordRoutes = require('./routes/record')
const categoryRoutes = require('./routes/category')

const corsOptions = {
    origin: ['http://localhost:3000'], 
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

mongoose.connect(process.env.DB_MONGODB, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection

db.on('error', () => {
	console.log('Something went wrong with our MongoDB connection')
})

db.once('open', () => {
	console.log('Now connected to cloud database')
})


app.use(express.json())
app.use(express.urlencoded({extended:true}))



app.use('/api/users', cors(corsOptions), userRoutes)
app.use('/api/records', cors(corsOptions), recordRoutes)
app.use('/api/categories', cors(corsOptions), categoryRoutes)



app.listen(port, ()=> {
	console.log(`API is now online on port ${port}`)
})