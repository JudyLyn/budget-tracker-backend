const Category = require('../models/category')
const User = require('../models/user')


//get one 
module.exports.getAll = ()=> {
	return Category
		.find({isActive: true})
		.sort({_id: -1})
		.then(categories => categories)
}


//add categories
module.exports.addCategory = (params) => {
	let category = new Category({ 
		'categoryName': params.categoryName, 
		'categoryType': params.categoryType
	});

	return category.save().then((item, err) => {
		return (err) ? false : true
	})
}

//archive or delete
module.exports.archive = (params) => {
	return Category.findById(params.categoryId)
	.then((category, err) => {
		if(err) return false;

			category.isActive = false;

			console.log(category);

			return category.save()
			.then((updatedCategory, err)=>{
				return err ? false : true
			})
	})
}


module.exports.getCategories = (params) => {
	console.log(params);

    return Category.find({categoryType: params.typeName, isActive: true}).then(category => {
       return category;
    })
}
