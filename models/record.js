const mongoose = require('mongoose')

const recordSchema = new mongoose.Schema({

	categoryName: {
		type: String,
		required: [true, 'Category name is required']
	},
	categoryType: {
		type: String,
		required: [true, 'Category type is required']
	},
	amount: {
		type: Number,
		required: [true, 'amount is required']
	},
	description: {
		type: String,
		default: null
	},
	currentBalance: {
		type: Number,
		required: [true, 'amount is required']
	},
			
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	users: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	}




})

module.exports = mongoose.model('record', recordSchema)