const mongoose = require('mongoose');
const RecordModel = require('./record.js');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
	},
	loginType:{
		type: String,
		required: [true, 'Login is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	records: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: "Record"
	}],
	categories: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: "Category"
	}]

})

module.exports = mongoose.model('user', userSchema)