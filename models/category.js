const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({

	categoryName:{
		type: String,
		required: [true, 'Category name is required']
	},
	categoryType:{
		type: String,
		
	},
	isActive: {
		type: Boolean,
		default: true
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	}
})

module.exports = mongoose.model('category', categorySchema)